//
//  ViewController.swift
//  Practica 1
//
//  Created by IDS Comercial on 12/01/22.
//

import UIKit

class ViewController: UIViewController {
    
    let info = ["DATO1","DATO2","DATO3","DATO4"]
    
    @IBOutlet weak var tableViewData: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableViewData.delegate = self
        tableViewData.dataSource = self
    }
}

extension ViewController: UITableViewDelegate{

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension ViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return info.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellList", for: indexPath)
        cell.textLabel?.text = info[indexPath.row]
       return cell

    }
}

