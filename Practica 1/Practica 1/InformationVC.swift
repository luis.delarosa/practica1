//
//  InformationVC.swift
//  Practica 1
//
//  Created by IDS Comercial on 12/01/22.
//

import Foundation
import UIKit

class InformationVC: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imageList: UIImageView!
    
    var img = UIImage()
    var user_name = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTitle.text = "Hey i am \(user_name)."
        imageList.image = img

    }
}
