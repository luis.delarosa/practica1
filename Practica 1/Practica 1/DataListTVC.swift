//
//  DataListTVC.swift
//  Practica 1
//
//  Created by IDS Comercial on 12/01/22.
//

import Foundation
import UIKit

class DataListTVC: UITableViewCell{
    
    @IBOutlet weak var lblTitle: UILabel!

    @IBOutlet weak var imageList: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
